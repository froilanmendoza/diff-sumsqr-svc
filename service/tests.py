from django.test import TestCase
from .models import Service

import json

ENDPOINT = '/difference?number='

class ServiceModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # dummy data testing
        Service.objects.create(number=1, occurrences=1)

    def test_occurrences_data(self):
        service = Service.objects.get(number=1)
        expected_object_name = f'{service.occurrences}'
        self.assertEqual(int(expected_object_name), 1)


class HomePageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Service.objects.create(number=1, occurrences=1)

    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

class DifferencePageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Service.objects.create(number=1, occurrences=1)

    def test_json_response(self):
        resp = self.client.get(('%s%d' % (ENDPOINT,1)))
        response_dict = json.loads(resp.content)
        self.assertEqual(response_dict['value'], 0)

    def test_json_valid_response_code(self):
        resp = self.client.get(('%s%d' % (ENDPOINT,1)))
        self.assertEqual(resp.status_code, 200)

    def test_json_error_response_text(self):
        resp = self.client.get(('%s%s' % (ENDPOINT,'a')))
        self.assertJSONEqual(
                str(resp.content, encoding='utf8'),
                {'status': 'error'}
        )

    def test_json_error_response_code(self):
        resp = self.client.get(('%s%s' % (ENDPOINT,'a')))
        self.assertEqual(resp.status_code, 400)

    def test_json_out_of_score_value_error_response(self):
        resp = self.client.get(('%s%d' % (ENDPOINT,101)))
        self.assertEqual(resp.status_code, 400)
