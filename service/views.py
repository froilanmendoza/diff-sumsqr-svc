from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import ListView
from .models import Service

import json

class HomePageView(ListView):
    model = Service
    template_name = 'home.html'

def DifferencePageView(request):
    model = Service
    number = Service.filterint(request.GET.get('number'))
    if not number:
        error_output = {
            'status': 'error',
        }
        return JsonResponse(error_output, status=400)
    
    try:
        obj = Service.objects.get(number=number)
        Service.updateOccurrence(Service, number, (obj.occurrences+1))
    except Service.DoesNotExist:
        obj = Service(number=number, occurrences=1)
        obj.save()

    output = Service.getSumSq(Service, number)

    #obj = Service.objects.get_or_create(number=number, defaults={'occurrences':1}) also returns True
    
    return JsonResponse(output, status=200)
