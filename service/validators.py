from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_service_value(value):
    if value < 0 or value >= 100:
        raise ValidationError(
                _('%(value)s is not valid number'),
                params={'value': value},
        )
    else:
        return value
