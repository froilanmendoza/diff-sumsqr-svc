from django.core.validators import MaxValueValidator
from django.db import models
from .validators import validate_service_value

import datetime

class Service(models.Model):
    number = models.PositiveSmallIntegerField(primary_key=True, validators=[validate_service_value])
    occurrences = models.PositiveIntegerField()

    def __str__(self):
        ''' a string representation '''
        output = ('%d => %d' % (self.number, self.occurrences)) 
        return str(output)

    def updateOccurrence(self, findThis, newVal):
        '''
        updates value of occurrences by number
        Args:
            findThis (int): number value
            newVal (int): occurrences new value

        '''
        q = self.objects.get(number=findThis)
        q.occurrences = newVal
        q.save()

    def getSumSq(self, number):
        '''
        sets the response format for square difference
        Args:
            number (int): number value

        Returns:
            dict of response values
        '''
        q = self.objects.get(number=number)
        response = { 
                'datetime': datetime.datetime.utcnow(),
                'value': self.getSumSqDifference(number),
                'number': q.number, 
                'occurrences': q.occurrences 
                }
        return response 

    @staticmethod
    def getSumSqDifference(number):
        '''
        gets the sum square difference 
        Args:
            number (int): number to be evaluated

        Returns:
            int: the sum of square of sum and sum of square of number

        TODO: runtime of this is O(n), maybe use better algorithm or 
        use actual translation of equation to math
        see: https://stackoverflow.com/questions/15593428/function-to-calculate-the-difference-between-sum-of-squares-and-square-of-sums
        resulting in runtime of O(1)
        '''
        total = 0
        sumOfSq = 0
        sqOfSum = 0
        difference = 0

        for i in range(1, number+1):
            total += i
            sumOfSq += i*i
            sqOfSum = total*total
            difference = sqOfSum - sumOfSq

        return difference

    @staticmethod
    def filterint(value):
        '''
        filter values received to 0 if non valid integer (0 < value <= 100)
        Args:
            value (int): number to compute sqSumDifference

        Returns:
            int: actual value or 0 (for invalid input)
        '''
        try:
            myval = int(value)
            if 0 < myval <= 100: 
                return myval
        except ValueError:
            return 0
        return 0
