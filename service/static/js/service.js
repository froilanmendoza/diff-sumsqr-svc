
$('#id_submit').on('click', function(e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8000/difference',
        data: { number: $('#id_number').val() },
        dataType: 'json',
        error: function() { 
            $("#error_blk").show();
            $("#success_blk").hide();
        },
        success: function(msg) { 
            $('#error_blk').hide(); 
            $('#success_blk').show();
            $('#datetime_row').html(msg['datetime']);
            $('#value_row').html(msg['value']);
            $('#number_row').html(msg['number']);
            $('#occurrences_row').html(msg['occurrences']);
        },
    })
});
