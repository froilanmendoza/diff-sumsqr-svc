# Get Square Sum Difference Service #
A service that outputs the difference between the sum of squares of the first n natural numbers and the square of the sum of the same first n natural numbers, where n is no greater than 100.  The output is in JSON format.

For more details, see specification.txt.

## Getting Started / Installation ##
To install:
    pip install -r requirements.txt

## Post-build ##
The default installation is running off of  Postgres database.  You may toggle into an sqlite installation by renaming the svc_project/settings-sqlite.py into svc_project/settings.py.

Default postgres database values are in settings-pg.py

## Tests ##
Tests cover valid and invalid input to the service.  To run:

./manage.py test

## Deployment / Demo ##
This project was built on Django 2.0.2/Python 3.6.2.  It has been tested on a Macbook Pro running in virtual environment using pipenv.  This was also deployed to Heroku and can be tested by going to: https://damp-waters-31147.herokuapp.com/

## Frontend ##
Frontend implementation via Bootstrap and jQuery available on root (http://localhost:8000).  Output displayed is based on calling the service

## Author ##
Froilan Mendoza <froilan@nyu.edu>

Version 0.1
 
